# My Song Lyric

This Project is for Android Candidate in Female Daily. This Project contains of :
* Login Google
* List Track Song
* Search Track
* Wish List
* Calendar Event
* Add New Event

## Getting Started

Clone the link project git to your computer and open the folder project in Android Studio.
Wait until the gradle finish build and indexing and you can run the app to your emulator or smarthphone

### Prerequisites

You need:
* Android Studio
* Android SDK
* JDK 1.8
* Kotlin Pluggin

## Built With

* Kotlin
* Koin Depedency Injection
* Kotlin Coroutin
* Realm DB for Android
* Google Service API for Sign In
* Retrofit

## Versioning

We use Version Name 1.0

## Authors

* **Dimas Prakoso - dimas.prakoso95@gmail.com**
