package com.mysonglyric.android.base

import android.app.Application
import com.mysonglyric.android.di.networkModule
import com.mysonglyric.android.di.prefferenceModule
import com.mysonglyric.android.di.songModule
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Dimas Prakoso on 05/12/2019.
 */
class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(mutableListOf(
                prefferenceModule,
                networkModule,
                songModule
            ))
        }

        //Realm configuration
        Realm.init(this)
        val configuration = RealmConfiguration.Builder()
            .name("my_song_app.db")
            .schemaVersion(1)
            .build()
        Realm.setDefaultConfiguration(configuration)
    }
}