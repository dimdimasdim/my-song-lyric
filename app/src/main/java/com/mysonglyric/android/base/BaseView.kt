package com.mysonglyric.android.base

import androidx.appcompat.widget.Toolbar

/**
 * Created by Dimas Prakoso on 04/12/2019.
 */
interface BaseView {

    fun setupToolbar(toolbar: Toolbar?, title: String, isChild: Boolean)

}