package com.mysonglyric.android.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

/**
 * Created by Dimas Prakoso on 04/12/2019.
 */
abstract class BaseActivity: AppCompatActivity(), BaseView {

    protected abstract val layoutResource: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
        initLib()
        initIntent()
        initUI()
        initAction()
        initProcess()
    }

    override fun setupToolbar(toolbar: Toolbar?, title: String, isChild: Boolean) {
        toolbar?.let {
            setSupportActionBar(toolbar)
        }
        supportActionBar?.let {
            it.title = title
            it.setDisplayHomeAsUpEnabled(isChild)
        }
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    open fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    protected abstract fun initLib()

    protected abstract fun initIntent()

    protected abstract fun initUI()

    protected abstract fun initAction()

    protected abstract fun initProcess()
}