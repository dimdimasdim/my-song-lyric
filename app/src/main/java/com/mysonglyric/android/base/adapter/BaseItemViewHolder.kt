package com.mysonglyric.android.base.adapter

import android.content.Context
import android.view.View
import com.mysonglyric.android.base.adapter.BaseRecyclerAdapter.OnItemClickListener
import com.mysonglyric.android.base.adapter.BaseRecyclerAdapter.OnLongItemClickListener

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
abstract class BaseItemViewHolder<Data>(protected var mContext: Context?,
    itemView: View,
    private val mItemClickListener: OnItemClickListener?,
    private val mLongItemClickListener: OnLongItemClickListener?)
    : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    var isHasHeader = false

    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    abstract fun bind(data: Data)

    override fun onClick(v: View) {
        mItemClickListener?.onItemClick(v, if (isHasHeader) adapterPosition - 1 else adapterPosition)
    }

    override fun onLongClick(v: View): Boolean {
        if (mLongItemClickListener != null) {
            mLongItemClickListener.onLongItemClick(v, if (isHasHeader) adapterPosition - 1 else adapterPosition)
            return true
        }
        return false
    }
}