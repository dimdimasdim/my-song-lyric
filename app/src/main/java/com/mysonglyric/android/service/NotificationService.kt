package com.mysonglyric.android.service

import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import com.google.gson.Gson
import com.mysonglyric.android.R
import com.mysonglyric.android.R.string
import com.mysonglyric.android.presentation.calendar.detailevent.DetailEventActivity
import com.mysonglyric.android.presentation.model.Event
import com.mysonglyric.android.utils.constants.BundleKeys
import java.util.Calendar

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
class NotificationService : IntentService("NotificationService") {

    private lateinit var mNotification: Notification
    private val mNotificationId: Int = 1000

    companion object {

        const val CHANNEL_ID = "om.mysonglyric.android.service.CHANNEL_ID"
        const val CHANNEL_NAME = "Event Notification"
    }

    private fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library

            val context = this.applicationContext
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description = getString(R.string.notification_channel_description)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        //Create Channel
        val gson = Gson()
        createChannel()
        var timestamp: Long = 0
        var event: Event? = null
        if (intent != null && intent.extras != null) {
            timestamp = intent.extras!!.getLong("timestamp")
            val jsonEvent = intent.extras?.getString("reason")
            event = gson.fromJson(jsonEvent, Event::class.java)
        }

        if (timestamp > 0) {

            val context = this.applicationContext
            val notifyIntent = Intent(this, DetailEventActivity::class.java)

            val title = event?.eventName.orEmpty()
            val message = getString(string.label_message_event_notif)

            notifyIntent.putExtra(BundleKeys.KEY_EVENT, event)
            notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            val pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val res = this.resources
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mNotification = Notification.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(
                        Notification.BigTextStyle()
                            .bigText(message)
                    )
                    .setContentText(message).build()
            } else {

                mNotification = Notification.Builder(this)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(title)
                    .setStyle(
                        Notification.BigTextStyle()
                            .bigText(message)
                    )
                    .setSound(uri)
                    .setContentText(message).build()
            }

            val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(mNotificationId, mNotification)
        }
    }
}