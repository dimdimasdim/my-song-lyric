package com.mysonglyric.android.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import com.mysonglyric.android.R
import com.mysonglyric.android.utils.ext.onClick
import com.mysonglyric.android.utils.ext.visible
import kotlinx.android.synthetic.main.layout_alert_dialog.view.btnAdd
import kotlinx.android.synthetic.main.layout_alert_dialog.view.btnCancel
import kotlinx.android.synthetic.main.layout_alert_dialog.view.tvMessage
import kotlinx.android.synthetic.main.layout_alert_dialog.view.tvTitle

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
fun showCancelableDialog(
    context: Context,
    title : String? = null,
    message : String? = null,
    positive: String? = null, @NonNull positiveListener: (() -> Unit)? = null,
    negative: String? = null, @NonNull negativeListener: (() -> Unit)? = null
) {
    val viewWithoutIcon : View = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog, null)

    with(viewWithoutIcon) {
        tvTitle.text = title

        if (message != null) {
            tvMessage.visible()
            tvMessage.text = message
        }

        if(positive != null) {
            btnAdd.visible()
            btnAdd.text = positive
        }

        if(negative != null) {
            btnCancel.visible()
            btnCancel.text = negative
        }
    }

    val builderWithoutIcon = AlertDialog.Builder(context).setView(viewWithoutIcon)
    val dialogWithoutIcon = builderWithoutIcon.create()

    dialogWithoutIcon.apply {
        window?.apply {
            setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            setGravity(Gravity.CENTER)
            val inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 60)
            setBackgroundDrawable(inset)
        }
        setCancelable(true)
        show()
    }

    with(viewWithoutIcon) {
        btnAdd.onClick {
            dialogWithoutIcon.dismiss()
            positiveListener?.invoke()
        }
        btnCancel.onClick {
            dialogWithoutIcon.dismiss()
            negativeListener?.invoke()
        }
    }
}