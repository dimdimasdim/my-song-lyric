package com.mysonglyric.android.utils.constants

/**
 * Created by Dimas Prakoso on 05/12/2019.
 */
object GoogleSignInConstans {
    const val GOOGLE_SIGN = 1505

    const val GOOGLE_TYPE = "com.google"
}