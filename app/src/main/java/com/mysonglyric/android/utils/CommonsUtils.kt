package com.mysonglyric.android.utils

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */

fun emptyString() = ""

fun Long.formatAlarm(): Long{
    return this - 180000
}