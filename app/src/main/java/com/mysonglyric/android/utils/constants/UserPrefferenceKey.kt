package com.mysonglyric.android.utils.constants

/**
 * Created by Dimas Prakoso on 05/12/2019.
 */
object UserPrefferenceKey {

    const val USER_EMAIL = "user_email"

    const val USER_NAME = "user_name"

    const val USER_LOGGED_IN = "user_logged_in"
}