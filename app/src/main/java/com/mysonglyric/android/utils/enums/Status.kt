package com.mysonglyric.android.utils.enums

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}