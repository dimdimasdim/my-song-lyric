package com.mysonglyric.android.utils.constants

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
object BundleKeys {

    const val KEY_EVENT = "key_event"

    const val KEY_DATE = "key_date"

    const val KEY_TIME = "key_time"

}