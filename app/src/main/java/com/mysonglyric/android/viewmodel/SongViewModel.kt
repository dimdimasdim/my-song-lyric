package com.mysonglyric.android.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.mysonglyric.android.data.song.SongRepository
import com.mysonglyric.android.networking.Resource
import kotlinx.coroutines.Dispatchers

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
class SongViewModel(
    private val songRepository: SongRepository
): ViewModel() {

    private val artistName = MutableLiveData<String>()

    fun getListSongs(artisName: String){
        this.artistName.value = artisName
    }

    var songs = artistName.switchMap {artistName ->
        liveData(Dispatchers.IO){
            emit(Resource.loading(null))
            emit(songRepository.getListSong(artistName))
        }
    }
}