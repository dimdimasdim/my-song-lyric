package com.mysonglyric.android.data.song.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
data class TrackItem(
    @SerializedName("track_id")
    val trackId: String?,
    @SerializedName("track_name")
    val trackName: String?,
    @SerializedName("artist_name")
    val artistName: String?,
    var isFavorite: Boolean = false
)