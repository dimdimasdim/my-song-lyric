package com.mysonglyric.android.data.song

import com.mysonglyric.android.data.song.model.SongItem
import com.mysonglyric.android.data.song.remote.SongApi
import com.mysonglyric.android.networking.Resource
import com.mysonglyric.android.networking.ResponseHandler
import java.lang.Exception

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
open class SongRepository(
    private val songApi: SongApi,
    private val responseHandler: ResponseHandler
) {

    suspend fun getListSong(artistName: String): Resource<SongItem>{
        return try {
            val response = songApi.getTrackListByArtisName(artistName)
            return responseHandler.handleSuccess(response)
        }catch (e: Exception){
            responseHandler.handleException(e)
        }
    }
}