package com.mysonglyric.android.data.event

import io.realm.RealmObject

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
open class EventEntity(
    var date: String? = null,
    var time: String? = null,
    var eventName: String? = null
): RealmObject()