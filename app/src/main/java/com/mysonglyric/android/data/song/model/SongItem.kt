package com.mysonglyric.android.data.song.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
data class SongItem(
    @SerializedName("message")
    val messageItem: MessageItem
)