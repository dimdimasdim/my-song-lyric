package com.mysonglyric.android.data.song.remote

import com.mysonglyric.android.data.song.model.SongItem
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
interface SongApi {

    @GET("track.search")
    suspend fun getTrackListByArtisName(
        @Query("q_artist") artisName: String
    ): SongItem

}