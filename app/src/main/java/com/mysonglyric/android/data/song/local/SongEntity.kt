package com.mysonglyric.android.data.song.local

import io.realm.RealmObject

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
open class SongEntity(
    var idSong: String? = null,
    var title: String? = null,
    var artistName: String? = null,
    var isFavorite: Boolean? = false
): RealmObject()