package com.mysonglyric.android.di

import com.mysonglyric.android.data.SharedPreference
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Created by Dimas Prakoso on 05/12/2019.
 */
val prefferenceModule = module {

    single { SharedPreference(androidContext()) }

}