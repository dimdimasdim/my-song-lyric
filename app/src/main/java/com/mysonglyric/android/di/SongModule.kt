package com.mysonglyric.android.di

import com.mysonglyric.android.data.song.SongRepository
import com.mysonglyric.android.data.song.remote.SongApi
import com.mysonglyric.android.viewmodel.SongViewModel
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
val songModule = module {
    factory { provideSongApi(get()) }
    factory { SongRepository(get(), get()) }
    factory { SongViewModel(get()) }
}

fun provideSongApi(retrofit: Retrofit): SongApi = retrofit.create(SongApi::class.java)