package com.mysonglyric.android.networking

import com.mysonglyric.android.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
class AuthInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val url = req.url.newBuilder().addQueryParameter("apikey", BuildConfig.API_KEY).build()
        req = req.newBuilder().url(url).build()
        return chain.proceed(req)
    }
}