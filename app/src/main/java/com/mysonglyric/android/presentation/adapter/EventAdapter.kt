package com.mysonglyric.android.presentation.adapter;

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.mysonglyric.android.R
import com.mysonglyric.android.base.adapter.BaseItemViewHolder
import com.mysonglyric.android.base.adapter.BaseRecyclerAdapter
import com.mysonglyric.android.presentation.adapter.EventAdapter.EventViewHolder
import com.mysonglyric.android.presentation.model.Event
import com.mysonglyric.android.utils.ext.onClick
import kotlinx.android.synthetic.main.item_event.view.tvEventDateTime
import kotlinx.android.synthetic.main.item_event.view.tvEventName

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
class EventAdapter(context: Context, data: MutableList<Event>) :
    BaseRecyclerAdapter<Event, EventViewHolder>(context, data) {

    var listener: OnEventItemListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return EventViewHolder(
            mContext,
            getView(parent, viewType),
            mItemClickListener,
            mLongItemClickListener
        )
    }

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_event

    inner class EventViewHolder(
        context: Context?,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        longItemClickListener: OnLongItemClickListener?
    ) : BaseItemViewHolder<Event>(context, itemView, itemClickListener, longItemClickListener) {

        override fun bind(data: Event) {
            with(itemView) {
                tvEventName.text = data.eventName
                tvEventDateTime.text = "${data.date} ${data.time}"

                itemView.onClick {
                    listener?.onItemEventClick(data)
                }
            }
        }
    }

    interface OnEventItemListener {

        fun onItemEventClick(data: Event)
    }
}