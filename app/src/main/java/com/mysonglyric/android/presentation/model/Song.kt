package com.mysonglyric.android.presentation.model

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
data class Song(
    val id: String,
    val title: String,
    val artistName: String,
    var isFavorite: Boolean = false
)