package com.mysonglyric.android.presentation.calendar.detailevent

import android.content.Context
import android.view.MenuItem
import com.mysonglyric.android.R
import com.mysonglyric.android.base.BaseActivity
import com.mysonglyric.android.presentation.model.Event
import com.mysonglyric.android.utils.constants.BundleKeys
import kotlinx.android.synthetic.main.activity_detail_event.tvEventDateTime
import kotlinx.android.synthetic.main.activity_detail_event.tvEventName
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import org.jetbrains.anko.startActivity

class DetailEventActivity : BaseActivity() {

    private var event: Event? = null

    companion object {
        fun start(context: Context, event: Event) {
            context.startActivity<DetailEventActivity>(
                BundleKeys.KEY_EVENT to event
            )
        }
    }

    override val layoutResource = R.layout.activity_detail_event

    override fun initLib() {
    }

    override fun initIntent() {
        event = intent?.getParcelableExtra(BundleKeys.KEY_EVENT)
    }

    override fun initUI() {
        setupToolbar(toolbar, getString(R.string.label_detail_event), true)
        event?.let {
            tvEventName.text = it.eventName
            tvEventDateTime.text = "${it.date} ${it.time}"
        }
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }
}
