package com.mysonglyric.android.presentation.adapter;

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.mysonglyric.android.R
import com.mysonglyric.android.R.string
import com.mysonglyric.android.base.adapter.BaseItemViewHolder
import com.mysonglyric.android.base.adapter.BaseRecyclerAdapter
import com.mysonglyric.android.presentation.adapter.SongAdapter.SongViewHolder
import com.mysonglyric.android.presentation.model.Song
import com.mysonglyric.android.utils.ext.invisible
import com.mysonglyric.android.utils.ext.onClick
import com.mysonglyric.android.utils.ext.visible
import kotlinx.android.synthetic.main.item_song.view.btnFavorite
import kotlinx.android.synthetic.main.item_song.view.tvArtistName
import kotlinx.android.synthetic.main.item_song.view.tvIdSong
import kotlinx.android.synthetic.main.item_song.view.tvSong

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
class SongAdapter(context: Context, data: MutableList<Song>) :
    BaseRecyclerAdapter<Song, SongViewHolder>(context, data) {

    var listener: OnSongItemListener? = null

    var isWishListType = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder {
        return SongViewHolder(
            mContext,
            getView(parent, viewType),
            mItemClickListener,
            mLongItemClickListener
        )
    }

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_song

    inner class SongViewHolder(
        context: Context?,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        longItemClickListener: OnLongItemClickListener?
    ) : BaseItemViewHolder<Song>(context, itemView, itemClickListener, longItemClickListener) {

        override fun bind(data: Song) {
            with(itemView) {
                tvIdSong.text = "ID Track : ${data.id}"
                tvSong.text = data.title
                tvArtistName.text = data.artistName
                if (data.isFavorite && isWishListType) {
                    btnFavorite.text = context.getString(string.label_remove_favorite)
                } else if (data.isFavorite && !isWishListType) {
                    btnFavorite.invisible()
                } else {
                    btnFavorite.visible()
                }

                btnFavorite.onClick {
                    if (!isWishListType)
                        listener?.onAddSongToFavorite(data, adapterPosition)
                    else listener?.onRemoveFavorite(data, adapterPosition)
                }
            }
        }
    }

    interface OnSongItemListener {

        fun onAddSongToFavorite(data: Song, adapterPosition: Int)

        fun onRemoveFavorite(data: Song, adapterPosition: Int)
    }
}