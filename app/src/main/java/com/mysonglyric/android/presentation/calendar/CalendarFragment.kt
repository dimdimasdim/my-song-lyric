package com.mysonglyric.android.presentation.calendar

import android.app.TimePickerDialog
import android.util.Log
import com.mysonglyric.android.R
import com.mysonglyric.android.base.BaseFragment
import com.mysonglyric.android.presentation.calendar.detailcalendar.DetailCalendarActivity
import com.mysonglyric.android.utils.emptyString
import kotlinx.android.synthetic.main.fragment_calendar.calenderView
import kotlinx.android.synthetic.main.fragment_calendar.tvSelectedDate
import java.util.Calendar

class CalendarFragment : BaseFragment() {

    private var date = emptyString()
    private var time = emptyString()

    override val layoutResource = R.layout.fragment_calendar

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
        calenderView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            date = (dayOfMonth.toString() + "/" + (month + 1) + "/" + year)
            tvSelectedDate.text = date
            openTimePicker()
        }
    }

    override fun initProcess() {
    }

    private fun openTimePicker() {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(context, TimePickerDialog.OnTimeSetListener(function = { _, selectedHour, selectedMinute ->
            time = String.format("%02d:%02d", selectedHour, selectedMinute)
            context?.let {
                DetailCalendarActivity.start(it, date, time)
            }
        }), hour, minute, false)
        tpd.show()
    }


}
