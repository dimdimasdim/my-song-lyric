package com.mysonglyric.android.presentation.calendar.detailcalendar

import android.content.Context
import android.view.MenuItem
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mysonglyric.android.R
import com.mysonglyric.android.R.string
import com.mysonglyric.android.base.BaseActivity
import com.mysonglyric.android.data.event.EventEntity
import com.mysonglyric.android.presentation.adapter.EventAdapter
import com.mysonglyric.android.presentation.adapter.EventAdapter.OnEventItemListener
import com.mysonglyric.android.presentation.calendar.detailevent.DetailEventActivity
import com.mysonglyric.android.presentation.model.Event
import com.mysonglyric.android.utils.constants.BundleKeys
import com.mysonglyric.android.utils.emptyString
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_detail_calendar.rvListEventCalender
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import org.jetbrains.anko.startActivity

class DetailCalendarActivity : BaseActivity(), OnEventItemListener {

    private lateinit var realm: Realm
    private lateinit var eventAdapter: EventAdapter

    private var date = emptyString()
    private var time = emptyString()

    companion object {
        fun start(context: Context, date: String, time: String) {
            context.startActivity<DetailCalendarActivity>(
                BundleKeys.KEY_DATE to date,
                BundleKeys.KEY_TIME to time
            )
        }
    }

    override val layoutResource = R.layout.activity_detail_calendar

    override fun initLib() {
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
    }

    override fun initIntent() {
        date = intent.getStringExtra(BundleKeys.KEY_DATE) ?: emptyString()
        time = intent.getStringExtra(BundleKeys.KEY_TIME) ?: emptyString()
    }

    override fun initUI() {
        setupToolbar(toolbar, getString(string.label_detail_event_list), true)
        eventAdapter = EventAdapter(this, mutableListOf())
        eventAdapter.listener = this
        rvListEventCalender.apply {
            layoutManager = LinearLayoutManager(this@DetailCalendarActivity)
            addItemDecoration(DividerItemDecoration(this@DetailCalendarActivity, LinearLayoutManager.VERTICAL))
            adapter = eventAdapter
        }
    }

    override fun initAction() {
    }

    override fun initProcess() {
        realm.executeTransactionAsync({ realm ->
            val eventEntity = realm.where(EventEntity::class.java).equalTo("date", date).findAll()
            showListDataEvent(eventEntity)
        }, {}, {})
    }

    private fun showListDataEvent(eventEntity: RealmResults<EventEntity>?) {
        val events = mutableListOf<Event>()
        eventEntity?.let {
            it.forEachIndexed { index, eventEntity ->
                events.add(
                    Event(
                        date = eventEntity.date.orEmpty(),
                        time = eventEntity.time.orEmpty(),
                        eventName = eventEntity.eventName.orEmpty()
                    )
                )
            }
        }
        eventAdapter.addOrUpdate(events)
    }

    override fun onItemEventClick(data: Event) {
        DetailEventActivity.start(this, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }
}
