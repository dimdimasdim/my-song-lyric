package com.mysonglyric.android.presentation

import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.mysonglyric.android.R.layout
import com.mysonglyric.android.base.BaseActivity
import com.mysonglyric.android.data.SharedPreference
import com.mysonglyric.android.utils.constants.GoogleSignInConstans
import com.mysonglyric.android.utils.constants.UserPrefferenceKey
import com.mysonglyric.android.utils.ext.onClick
import kotlinx.android.synthetic.main.activity_login.btnLoginGoogle
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity() {

    private val preference: SharedPreference by inject()

    override val layoutResource = layout.activity_login

    private lateinit var googleSignInClient: GoogleSignInClient

    companion object {
        fun start(context: Context) {
            context.startActivity<LoginActivity>()
        }
    }

    override fun initLib() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
        btnLoginGoogle.onClick {
            prepareLoginGoogle()
        }
    }

    private fun prepareLoginGoogle() {
        googleSignInClient.signOut().addOnSuccessListener { doLoginGoogle() }
            .addOnFailureListener { e -> showToast(e.message.toString()) }
    }

    private fun doLoginGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, GoogleSignInConstans.GOOGLE_SIGN)
    }

    override fun initProcess() {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GoogleSignInConstans.GOOGLE_SIGN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInGoogleResult(task)
        }
    }

    private fun handleSignInGoogleResult(task: Task<GoogleSignInAccount>?) {
        try {
            val account = task?.getResult(ApiException::class.java)
            Log.d("login google", "isi account ${account?.email} ${account?.displayName}")
            preference.save(UserPrefferenceKey.USER_EMAIL, account?.email.orEmpty())
            preference.save(UserPrefferenceKey.USER_NAME, account?.displayName.orEmpty())
            preference.save(UserPrefferenceKey.USER_LOGGED_IN, true)
            MainActivity.start(this)
            finish()
        } catch (e: ApiException) {
            showToast(e.message.toString())
        }
    }
}
