package com.mysonglyric.android.presentation.create

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.text.InputType
import android.util.Log
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import com.google.gson.Gson
import com.mysonglyric.android.R
import com.mysonglyric.android.R.string
import com.mysonglyric.android.base.BaseFragment
import com.mysonglyric.android.data.event.EventEntity
import com.mysonglyric.android.presentation.model.Event
import com.mysonglyric.android.utils.NotificationUtils
import com.mysonglyric.android.utils.emptyString
import com.mysonglyric.android.utils.ext.onClick
import com.mysonglyric.android.utils.ext.onTextChanged
import com.mysonglyric.android.utils.formatAlarm
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_create.btnCreate
import kotlinx.android.synthetic.main.fragment_create.edtDate
import kotlinx.android.synthetic.main.fragment_create.edtEventName
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

class CreateFragment : BaseFragment() {

    private lateinit var realm: Realm

    private var date = emptyString()
    private var time = emptyString()
    private var eventName = emptyString()
    private var gson = Gson()

    private val mNotificationTime =
        Calendar.getInstance().timeInMillis + 5000 //Set after 5 seconds from the current time.
    private var mNotified = false

    private val combinedCal = GregorianCalendar(TimeZone.getTimeZone("Asia/Jakarta"))

    override val layoutResource = R.layout.fragment_create

    override fun initLib() {
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
    }

    override fun initIntent() {
    }

    override fun initUI() {
        edtDate.inputType = InputType.TYPE_NULL
    }

    override fun initAction() {
        edtDate.onClick {
            showDatePicker()
        }

        btnCreate.onClick {
            preapareForm()
        }

        edtDate.onTextChanged {
            edtDate.error = null
        }
    }

    private fun preapareForm() {
        eventName = edtEventName.text.toString().trim()
        if (date.isEmpty() || time.isEmpty()){
            edtDate.error = getString(string.error_date_time_required)
            return
        }

        if (eventName.isEmpty()){
            edtEventName.error = getString(string.error_message_event_name)
            return
        }
        saveNewEvent()
    }

    override fun initProcess() {
    }

    private fun showDatePicker() {
        context?.let {
            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(
                it,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val dateSelected = Calendar.getInstance()
                    dateSelected.set(year, monthOfYear, dayOfMonth)
                    combinedCal.set(year, monthOfYear, dayOfMonth)
                    date = String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year)
                    openTimePicker()

                }, year, month, day
            )
            datePickerDialog.datePicker.minDate = calendar.timeInMillis
            datePickerDialog.show()
        }
    }

    private fun openTimePicker() {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener(function = { _, selectedHour, selectedMinute ->
                time = String.format("%02d:%02d", selectedHour, selectedMinute)
                combinedCal.set(Calendar.HOUR_OF_DAY, selectedHour)
                combinedCal.set(Calendar.MINUTE, selectedMinute)
                edtDate.setText("$date $time")
            }),
            hour,
            minute,
            false
        )
        tpd.show()
    }

    private fun saveNewEvent() {
        realm.executeTransactionAsync({ realm ->
            val eventEntity = realm.createObject(EventEntity::class.java)
            eventEntity.date = date
            eventEntity.time = time
            eventEntity.eventName = eventName
        }, {
            startAlarmManager(
                Event(
                    date = date,
                    time = time,
                    eventName = eventName
                )
            )
            resetFormEvent()
        }, {
            Log.d("realm", "On Error: Error in saving Data! ${it.cause}")
        })
    }

    private fun startAlarmManager(event: Event) {
        activity?.let {
            NotificationUtils().setNotification(
                combinedCal.timeInMillis.formatAlarm()
                , gson.toJson(event),
                it
            )
        }
    }

    private fun resetFormEvent() {
        edtDate.setText(emptyString())
        edtEventName.setText(emptyString())
    }
}
