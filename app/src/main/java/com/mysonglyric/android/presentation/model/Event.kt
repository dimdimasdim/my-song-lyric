package com.mysonglyric.android.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Dimas Prakoso on 07/12/2019.
 */
@Parcelize
data class Event(
    var date: String,
    var time: String,
    var eventName: String
): Parcelable