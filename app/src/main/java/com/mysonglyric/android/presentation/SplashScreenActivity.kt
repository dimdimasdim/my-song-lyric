package com.mysonglyric.android.presentation

import android.content.Context
import android.content.Intent
import com.mysonglyric.android.R.layout
import com.mysonglyric.android.base.BaseActivity
import com.mysonglyric.android.data.SharedPreference
import com.mysonglyric.android.utils.constants.UserPrefferenceKey
import org.koin.android.ext.android.inject
import java.util.Timer
import java.util.TimerTask

class SplashScreenActivity : BaseActivity() {

    private val sharedPreference: SharedPreference by inject()

    companion object {
        fun restart(context: Context) {
            val intent = Intent(context, SplashScreenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    override val layoutResource = layout.activity_splash_screen

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
    }

    override fun initProcess() {
        Timer().schedule(object : TimerTask() {
            override fun run() {
                toNextActivity()
            }
        }, 5000)
    }

    private fun toNextActivity() {
        val isLoggedIn = sharedPreference.getValueBoolien(UserPrefferenceKey.USER_LOGGED_IN, false)
        if (isLoggedIn)
            MainActivity.start(this)
        else
            LoginActivity.start(this)

        finish()
    }
}
