package com.mysonglyric.android.presentation

import android.content.Context
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.mysonglyric.android.R
import com.mysonglyric.android.R.string
import com.mysonglyric.android.base.BaseActivity
import com.mysonglyric.android.base.BaseFragment
import com.mysonglyric.android.data.SharedPreference
import com.mysonglyric.android.presentation.calendar.CalendarFragment
import com.mysonglyric.android.presentation.create.CreateFragment
import com.mysonglyric.android.presentation.find.FindFragment
import com.mysonglyric.android.presentation.find.FindFragment.OnFindFragmentListener
import com.mysonglyric.android.presentation.logout.LogoutFragment
import com.mysonglyric.android.presentation.wishlist.WishListFragment
import com.mysonglyric.android.utils.constants.UserPrefferenceKey
import com.mysonglyric.android.utils.emptyString
import com.mysonglyric.android.utils.showCancelableDialog
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.bnvMain
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity(), OnNavigationItemSelectedListener, OnFindFragmentListener {

    private val sharedPreference: SharedPreference by inject()
    private lateinit var realm: Realm

    companion object {
        fun start(context: Context) {
            context.startActivity<MainActivity>()
        }
    }

    private val findFragment = FindFragment()
    private val wishListFragment = WishListFragment()
    private val calendarFragment = CalendarFragment()
    private val createFragment = CreateFragment()

    private var userName = emptyString()
    private var userEmail = emptyString()

    override val layoutResource = R.layout.activity_main

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, getString(R.string.menu_find), false)
        selectFragment(findFragment)
    }

    override fun initAction() {
        bnvMain.setOnNavigationItemSelectedListener(this)
    }

    override fun initProcess() {
        findFragment.listener = this
        userName = sharedPreference.getValueString(UserPrefferenceKey.USER_NAME)
        userEmail = sharedPreference.getValueString(UserPrefferenceKey.USER_EMAIL)
    }

    private fun selectFragment(currentFragment: BaseFragment) {
        changeFragment(R.id.flMain, currentFragment)
    }

    private fun changeFragment(viewRes: Int, fragment: BaseFragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(viewRes, fragment)
            .addToBackStack(fragment.tag)
            .commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.isChecked = true
        when (item.itemId) {
            R.id.menuHome -> {
                supportActionBar?.title = getString(R.string.menu_find)
                selectFragment(findFragment)
                return true
            }
            R.id.menuWishlist -> {
                supportActionBar?.title = getString(R.string.menu_wishlist)
                selectFragment(wishListFragment)
                return true
            }
            R.id.menuCalendar -> {
                supportActionBar?.title = getString(R.string.menu_calendar)
                selectFragment(calendarFragment)
                return true
            }
            R.id.menuCreate -> {
                supportActionBar?.title = getString(R.string.label_create_event)
                selectFragment(createFragment)
                return true
            }
            R.id.menuLogout -> {
                showCancelableDialog(
                    this, getString(R.string.menu_logout),
                    String.format(getString(string.message_logout), userName, userEmail),
                    getString(string.label_yes),{
                        clearSeasonsUser()
                    },getString(string.label_no),{

                    }
                )
                return false
            }
        }

        return false
    }

    private fun clearSeasonsUser() {
        sharedPreference.clearSharedPreference()
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
        SplashScreenActivity.restart(this)
    }

    override fun onHideKeyBoard() {
        hideKeyboard(this)
    }
}
