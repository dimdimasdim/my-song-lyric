package com.mysonglyric.android.presentation.find

import android.util.Log
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mysonglyric.android.R
import com.mysonglyric.android.base.BaseFragment
import com.mysonglyric.android.data.song.local.SongEntity
import com.mysonglyric.android.data.song.model.SongItem
import com.mysonglyric.android.networking.Resource
import com.mysonglyric.android.presentation.adapter.SongAdapter
import com.mysonglyric.android.presentation.adapter.SongAdapter.OnSongItemListener
import com.mysonglyric.android.presentation.model.Song
import com.mysonglyric.android.utils.enums.Status.ERROR
import com.mysonglyric.android.utils.enums.Status.LOADING
import com.mysonglyric.android.utils.enums.Status.SUCCESS
import com.mysonglyric.android.utils.ext.gone
import com.mysonglyric.android.utils.ext.visible
import com.mysonglyric.android.viewmodel.SongViewModel
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_find.edtSearch
import kotlinx.android.synthetic.main.fragment_find.pbListTrack
import kotlinx.android.synthetic.main.fragment_find.rvFind
import kotlinx.android.synthetic.main.layout_info_page.containerInfoPage
import kotlinx.android.synthetic.main.layout_info_page.tvMessage
import kotlinx.android.synthetic.main.layout_info_page.tvTitle
import org.koin.androidx.viewmodel.ext.android.viewModel

class FindFragment : BaseFragment(), OnSongItemListener {

    private val songViewModel: SongViewModel by viewModel()

    private lateinit var songAdapter: SongAdapter

    private lateinit var realm: Realm

    private lateinit var songEntity: RealmResults<SongEntity>

    override val layoutResource = R.layout.fragment_find

    var listener: OnFindFragmentListener? = null

    override fun initLib() {
        songViewModel.songs.observe(this, Observer<Resource<SongItem>> {
            when(it.status){
                SUCCESS ->{
                    pbListTrack.gone()
                    showListData(it.data)
                }
                ERROR ->{}
                LOADING ->{
                    pbListTrack.visible()
                }
            }
        })

        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
    }

    private fun showListData(data: SongItem?) {
        data?.let {
            val songs = mutableListOf<Song>()
            val trackList = it.messageItem.body.trackList
            if (trackList.isNotEmpty()){
                containerInfoPage.gone()
                trackList.forEachIndexed { _, item ->
                    songEntity.forEach { entity ->
                        if (entity.idSong == item.track.trackId){
                            item.track.isFavorite = entity.isFavorite ?: false
                        }
                    }
                    songs.add(
                        Song(
                            id = item.track.trackId.orEmpty(),
                            title = item.track.trackName.orEmpty(),
                            artistName = item.track.artistName.orEmpty(),
                            isFavorite = item.track.isFavorite
                        )
                    )
                }
                songAdapter.addOrUpdate(songs)
            }else{
                containerInfoPage.visible()
            }

        }
    }

    override fun initIntent() {
    }

    override fun initUI() {
        containerInfoPage.visible()
        tvTitle.text = getString(R.string.menu_find)
        tvMessage.text = getString(R.string.messsage_empty)
        context?.let {
            songAdapter = SongAdapter(it, mutableListOf())
            songAdapter.listener = this
            rvFind.apply {
                layoutManager = LinearLayoutManager(it)
                addItemDecoration(DividerItemDecoration(it, LinearLayoutManager.VERTICAL))
                adapter = songAdapter
            }
        }
    }

    override fun initAction() {
        edtSearch.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch()
                return@OnEditorActionListener true
            }
            false
        })
    }

    override fun initProcess() {
        songEntity = realm.where(SongEntity::class.java).findAll()
    }

    private fun performSearch() {
        val queryArtis = edtSearch.text.toString().trim()
        if (queryArtis.isNotEmpty()){
            songViewModel.getListSongs(queryArtis)
            listener?.onHideKeyBoard()
            songAdapter.clear()
        } else{
            context?.let {
                Toast.makeText(it, "Input artist name first", Toast.LENGTH_SHORT)
            }
        }
    }

    override fun onAddSongToFavorite(data: Song, adapterPosition: Int) {
        realm.executeTransactionAsync({
            val songEntity = it.createObject(SongEntity::class.java)
            with(data){
                songEntity.idSong = id
                songEntity.title = title
                songEntity.artistName = artistName
                songEntity.isFavorite = true
            }
        },{
            Log.d("realm","On Success: Data Written Successfully!")
            data.isFavorite = true
            songAdapter.notifyItemChanged(
                adapterPosition,
                data
            )
        },{
            Log.d("realm","On Error: Error in saving Data! ${it.cause}")
        })
    }

    override fun onRemoveFavorite(data: Song, adapterPosition: Int) {
        Log.d("remove", " Remove song")
    }

    interface OnFindFragmentListener{
        fun onHideKeyBoard()
    }
}
