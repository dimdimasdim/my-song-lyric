package com.mysonglyric.android.presentation.logout

import com.mysonglyric.android.R
import com.mysonglyric.android.base.BaseFragment

class LogoutFragment : BaseFragment() {

    override val layoutResource = R.layout.fragment_logout

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }
}
