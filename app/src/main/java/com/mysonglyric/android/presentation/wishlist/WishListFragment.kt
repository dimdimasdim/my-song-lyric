package com.mysonglyric.android.presentation.wishlist

import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mysonglyric.android.R
import com.mysonglyric.android.base.BaseFragment
import com.mysonglyric.android.data.song.local.SongEntity
import com.mysonglyric.android.presentation.adapter.SongAdapter
import com.mysonglyric.android.presentation.adapter.SongAdapter.OnSongItemListener
import com.mysonglyric.android.presentation.model.Song
import com.mysonglyric.android.utils.ext.gone
import com.mysonglyric.android.utils.ext.visible
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_wish_list.rvWishlist
import kotlinx.android.synthetic.main.layout_info_page.containerInfoPage
import kotlinx.android.synthetic.main.layout_info_page.imgInfo
import kotlinx.android.synthetic.main.layout_info_page.tvMessage
import kotlinx.android.synthetic.main.layout_info_page.tvTitle

class WishListFragment : BaseFragment(), OnSongItemListener {

    private lateinit var realm: Realm
    private lateinit var songAdapter: SongAdapter

    override val layoutResource = R.layout.fragment_wish_list

    override fun initLib() {
        realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.commitTransaction()
    }

    override fun initIntent() {

    }

    override fun initUI() {
        containerInfoPage.visible()
        imgInfo.setImageResource(R.drawable.ic_heart)
        tvTitle.text = getString(R.string.menu_wishlist)
        tvMessage.text = getString(R.string.messsage_empty)
        context?.let {
            songAdapter = SongAdapter(it, mutableListOf())
            songAdapter.listener = this
            songAdapter.isWishListType = true
            rvWishlist.apply {
                layoutManager = LinearLayoutManager(it)
                addItemDecoration(DividerItemDecoration(it, LinearLayoutManager.VERTICAL))
                adapter = songAdapter
            }
        }
    }

    override fun initAction() {

    }

    override fun initProcess() {
        val songsEntity = realm.where(SongEntity::class.java).findAll()
        val wishList = mutableListOf<Song>()
        if (songsEntity.isNotEmpty()){
            containerInfoPage.gone()
            songsEntity.forEachIndexed { _, songEntity ->
                wishList.add(
                    Song(
                        id = songEntity.idSong.orEmpty(),
                        title = songEntity.title.orEmpty(),
                        artistName = songEntity.artistName.orEmpty(),
                        isFavorite = songEntity.isFavorite ?: false
                    )
                )
            }
            songAdapter.addOrUpdate(wishList)
        } else{
            containerInfoPage.visible()
        }
    }

    override fun onAddSongToFavorite(data: Song, adapterPosition: Int) {
        Log.d("add", " add song to favorite")
    }

    override fun onRemoveFavorite(data: Song, adapterPosition: Int) {
        realm.executeTransactionAsync ({ realm ->
            val rows: RealmResults<SongEntity> =
                realm.where(SongEntity::class.java).equalTo("idSong",data.id).findAll()
            rows.deleteFirstFromRealm()
        },{
            Log.d("realm","On Success: Data delete Successfully!")
            songAdapter.remove(adapterPosition)
        },{
            Log.d("realm","On Error: Error in delete Data! ${it.cause}")
        })
    }
}
